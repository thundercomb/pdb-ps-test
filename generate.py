from aitextgen import aitextgen

prefix = "Flames in the snow"

ai = aitextgen(model="trained_model/pytorch_model.bin", config="trained_model/config.json", to_gpu=False)
ai.generate(n=1,
            max_length=250,
            prompt=prefix,
            temperature=1.0,
            top_k=1000,
            top_p=0.9,
            num_beams=2,
            repetition_penalty=1.3
)
