FROM tensorflow/tensorflow:1.14.0-py3
COPY requirements.txt /
COPY app.py /
COPY templates/* /templates/
COPY trained_model/* /trained_model/
WORKDIR /
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3", "app.py"]
