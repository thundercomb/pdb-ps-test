from aitextgen import aitextgen
import logging

logging.basicConfig(
        format="%(asctime)s — %(levelname)s — %(name)s — %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO
    )

ai = aitextgen(tf_gpt2="124M", to_gpu=False)
file_name = '/Users/maartens/Documents/data/tinyshakespeare/tinyshakespeare.txt'

ai.train(file_name,
         line_by_line=False,
         from_cache=False,
         num_steps=1000,
         generate_every=1000,
         save_every=1000,
         save_gdrive=False,
         learning_rate=1e-4,
         batch_size=1,
)
